/*
  Copyright (C) 2016 Matthis Kruse

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DataConverter.hpp"

#include <stdexcept>
#include <sstream>
#include <limits>
#include <cmath>

std::istream& operator>>(std::istream& is, Vector2& v)
{
  std::string str;
  std::getline(is, str, ',');
  v.x = std::round(std::stof(str));

  std::getline(is, str, ' ');
  v.y = std::round(std::stof(str));
  
  return is;
}

Vector2::operator Json::Value() const noexcept
{
  Json::Value val;

  val["x"] = x;
  val["y"] = y;

  return val;
}

DataConverter::DataConverter(const std::string& d)
  : data()
{
  constexpr const float infty = std::numeric_limits<float>::infinity();
  
  process({infty, infty}, {infty, infty}, d, 0, 0);
}

DataConverter::operator Json::Value() const noexcept
{
  return data;
}

void DataConverter::process(Vector2 ref, Vector2 last, std::string str,
			    char mode, int num)
{
  if(str.empty()) 
    return;
  if(str == "z" || str == "Z")
  {
    //close path

    return;
  }

  std::string left = str;

  if(mode == 0)
  {
    mode = str.front();

    {
    const std::size_t pos = str.find_first_of(' ');
    if(pos == std::string::npos)
      throw std::invalid_argument("Data is corrupt");

    left.erase(0, pos+1);
    }

    std::stringstream ss(left);
    ss >> ref;
    left = ss.str();

    {
    const std::size_t pos = left.find_first_of(' ');
    if(pos == std::string::npos)
      throw std::invalid_argument("Data is corrupt");

    left.erase(0, pos+1);
    }
    bool cont = false;
    for(Json::ArrayIndex i = 0U; i < num; ++i)
        if(data[i]["x"].asInt() == ref.x && data[i]["y"].asInt() == ref.y)
            cont = true;
    if(!cont)
    {
        data[num++] = static_cast<Json::Value>(ref);
        last = ref;
    }
  }
  else
  {
    if(std::string("mMlLzZ").find(mode) == std::string::npos)
      throw std::invalid_argument("Data is not supported");
    
    const bool relative = (mode == 'm' || mode == 'l' || mode == 'z');

    if(std::string("lLzZ").find(left.front()) != std::string::npos)
    {
      mode = left.front();

      {
      const std::size_t pos = str.find_first_of(' ');
      if(pos == std::string::npos)
        throw std::invalid_argument("Data is corrupt");

      left.erase(0, pos+1);
      }
    }
    
    std::stringstream ss(left);
    
    Vector2 tmp{0.f, 0.f};
    ss >> tmp;
    
    {
    const std::size_t pos = left.find_first_of(' ');
    if(pos == std::string::npos)
      throw std::invalid_argument("Data is corrupt");

    left.erase(0, pos+1);
    }

    if(relative)
    {
      tmp.x += last.x;
      tmp.y += last.y;
    }

    bool cont = false;
    for(Json::ArrayIndex i = 0U; i < num; ++i)
        if(data[i]["x"].asInt() == tmp.x && data[i]["y"].asInt() == tmp.y)
            cont = true;
    if(!cont)
    {
        data[num++] = static_cast<Json::Value>(tmp);
        last = tmp;
    }
  }
  
  process(ref, last, left, mode, num);
}
