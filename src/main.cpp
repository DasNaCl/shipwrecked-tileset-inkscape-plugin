/*
  Copyright (C) 2016 Matthis Kruse

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "pugixml.hpp"
#include "json/json.h"
#include "DataConverter.hpp"

#include <iostream>
#include <string>
#include <vector>

Json::Value jsonValue;

struct simple_walker: pugi::xml_tree_walker
{
  bool for_each(pugi::xml_node& node) override
  {
    if(std::string(node.name()) != "g" && node.child("g").empty())
      return true;

    for(pugi::xml_node n = node.child("path"); n; n = n.next_sibling("path"))
    {
      if(std::string(n.child("title").child_value()) == "REF")
      {
	Json::Value data = DataConverter(n.attribute("d").value());
	jsonValue[node.attribute("id").value()]["REF"] = data[0];
      }
    }

    for(pugi::xml_node n = node.child("path"); n; n = n.next_sibling("path"))
    {
      if(std::string(n.child("title").child_value()) == "REF")
	continue;
      //new component
      Json::Value data = DataConverter(n.attribute("d").value());
	
      if(!node.attribute("id").empty() && n.child("title"))
      {
	jsonValue[node.attribute("id").value()][n.child("title").child_value()] = data;
      }
    }

    return true; // continue traversal
  }
};

int main(int argc, char* argv[])
{
  if(argc == 1)
  {
    std::cerr << "No Inkscape-SVG file given." << std::endl;
    
    return 1;
  }
  else if(argc > 2)
  {
    std::cerr << "Too many arguments." << std::endl;

    return 2;
  }
  else
  {
    pugi::xml_document doc;
    doc.load_file(std::string(argv[1]).c_str());

    simple_walker walker;
    doc.traverse(walker);
    
    std::cout << jsonValue << std::endl;
  }

  return 0;
}
